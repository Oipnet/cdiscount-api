Mise en place de l'app client Cdiscount
==============

Recupération de l'app
---------
> git clone https://framagit.org/Oipnet/cdiscount-api.git

Récupération des dépendences
------------
> composer install

Configuration
---------
Dans le fichier app/Controllers/OrderController.php remplacer les constantes LOGIN et PASSWORD avec les bonnes valeurs

Lancement d'un serveur local
-----------
> php -S localhost:8000 -t public/