<?php

namespace App\Middlewares;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class EloquentMiddleware implements Middleware {
    /**
     * @var Container
     */
    private $container;
    /**
     * @var array
     */
    private $database;


    /**
     * FlashMiddleware constructor.
     * @param array $database
     */
    public function __construct(Array $database)
    {
        $this->database = $database;
    }

    public function __invoke(Request $request, Response $response, $next)
    {
        $capsule = new \Illuminate\Database\Capsule\Manager();
        $capsule->addConnection($this->database);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        return $next($request, $response);
    }
}