<?php

namespace App\Controllers;

use App\Models\CDiscountApiModel;
use Carbon\Carbon;
use Slim\Http\Request;
use Slim\Http\Response;

class UploadController extends Controller {
    const CACHE_ORDER_TIME = 2;
    private $cdiscountApiModel;

    public function __construct($container)
    {
        parent::__construct($container);

        $this->cdiscountApiModel = new CDiscountApiModel('oki@okiwoki.com-api', 'api@cdiscount42');
        $this->cdiscountApiModel->getToken();
    }

    public function index(Request $request, Response $response) {
        $this->cdiscountApiModel->getModelList('100A0301');

        /// file_put_contents('temp.xml', $this->cdiscountApiModel->getLastResult()->GetModelListResult->ModelList->ProductModel->ProductXmlStructure);
        return $this->render($response, 'upload/index.twig');
    }

    public function send(Request $request, Response $response) {

        return $this->redirect($response, 'upload.index');
    }
}