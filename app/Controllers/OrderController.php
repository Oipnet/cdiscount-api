<?php

namespace App\Controllers;

use App\Models\CDiscountApiModel;
use Carbon\Carbon;
use Slim\Http\Request;
use Slim\Http\Response;

class OrderController extends Controller {
    const CACHE_ORDER_TIME = 2;
    const LOGIN = 'your-login';
    const PASSWORD = 'your-password';

    private $cdiscountApiModel;

    public function __construct($container)
    {
        parent::__construct($container);

        $this->cdiscountApiModel = new CDiscountApiModel(self::LOGIN, self::PASSWORD);
        $this->cdiscountApiModel->getToken();
    }

    public function index(Request $request, Response $response) {
        $orders = (isset($_SESSION['orders']))?$_SESSION['orders']:null;
        if (! isset($_SESSION['orders']) || $_SESSION['orders.date']->diffInMinutes(Carbon::now()) >= self::CACHE_ORDER_TIME) {
            $this->cdiscountApiModel->getOrderList();
            $orders = $this->cdiscountApiModel->getLastResult()->GetOrderListResult->OrderList->Order;
            $_SESSION['orders'] = $orders;
            $_SESSION['orders.date'] = Carbon::now();
        }

        return $this->render($response, 'orders/list.twig', compact('orders'));
    }

    public function show(Request $request, Response $response, $args) {
        if (!isset($_SESSION['orders'])) {
            return $this->redirect($response, 'order.index');
        }
        $order = $this->getOrder($_SESSION['orders'], $args['id']);
        return $this->render($response, 'orders/show.twig', compact('order'));
    }

    private function getOrder($orders, $id)
    {
        return array_filter($orders, function ($order) use ($id) {
            return $order->OrderNumber === $id;
        })[0];
    }

    public function refresh(Request $request, Response $response) {
        try {
            $this->cdiscountApiModel->getOrderList();
            $orders = $this->cdiscountApiModel->getLastResult()->GetOrderListResult->OrderList->Order;
            $_SESSION['orders'] = $orders;
            $_SESSION['orders.date'] = Carbon::now();
        } catch (SoapFault $exception) {
            return $response->withStatus(500);
        }

        return $response->withStatus(200)->withJson($orders);
    }
}