<?php
namespace App\Models;

use App\Libs\CDiscountWSDL;

/**
 * @link      https://github.com/basselin/php-seller-cdiscount-wsdl
 * @copyright (c) 2014-2015, Benoit Asselin contact(at)ab-d.fr
 * @license   MIT Licence
 */

class CDiscountApiModel extends CDiscountWSDL
{
    /**
     * URL du token
     * @const string
     */
    const URL = 'https://sts.cdiscount.com/users/httpIssue.svc/?realm=https://wsvc.cdiscount.com/MarketplaceAPIService.svc';

    /**
     * URL du webservice
     * @const string
     */
    const WSDL = 'https://wsvc.cdiscount.com/MarketplaceAPIService.svc?wsdl';
}
