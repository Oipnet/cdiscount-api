<?php

$app->get('/', \App\Controllers\OrderController::class.':index')->setName('order.index');
$app->get('/orders/refresh', \App\Controllers\OrderController::class.':refresh')->setName('order.refresh');

$app->get('/orders/{id}', \App\Controllers\OrderController::class.':show')->setName('order.show');

$app->get('/upload', \App\Controllers\UploadController::class.':index')->setName('upload.index');
$app->post('/upload', \App\Controllers\UploadController::class.':send')->setName('upload.index');