(function () {
    'use strict'

    var getHttpRequest = function () {
        var httpRequest = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            httpRequest = new XMLHttpRequest();
            if (httpRequest.overrideMimeType) {
                httpRequest.overrideMimeType('text/xml');
            }
        }
        else if (window.ActiveXObject) { // IE
            try {
                httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {}
            }
        }

        if (!httpRequest) {
            alert('Abandon :( Impossible de créer une instance XMLHTTP');
            return false;
        }

        return httpRequest
    }

    console.log('start refresh');
    window.setInterval(function () {
        var xhr = getHttpRequest()

        xhr.open('GET', '/orders/refresh')
        xhr.setRequestHeader('X-Requested-With', 'xmlhttprequest')
        xhr.send()

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                // validation ok avec retour http 200
                if (xhr.status === 200) {
                    console.log('Order list cached')
                }
            }
        }
    }, 2 * 60 * 1000)
})()